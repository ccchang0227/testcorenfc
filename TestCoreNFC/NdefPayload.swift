//
//  NdefPayload.swift
//  NFCTagReader
//
//  Created by realtouchapp on 2019/2/12.
//  Copyright © 2019年 Apple. All rights reserved.
//

import CoreNFC


extension NFCTypeNameFormat {
    
    var name: String {
        get {
            switch self {
            case .empty:
                return "Empty"
            case .nfcWellKnown:
                return "NFC Well Known"
            case .media:
                return "Media"
            case .absoluteURI:
                return "Absolute URI"
            case .nfcExternal:
                return "NFC External"
            case .unknown:
                return "Unknown"
            case .unchanged:
                return "Unchanged"
            }
        }
    }
    
}

open class NdefPayload : NSObject {
    open var typeNameFormat: NFCTypeNameFormat = .unknown
    open var type: String?
    
    public override init() {
        super.init()
    }
    
    public init(_ payload: NFCNDEFPayload) {
        super.init()
        
        setPayload(payload)
        
    }
    
    public func setPayload(_ payload: NFCNDEFPayload) {
        typeNameFormat = payload.typeNameFormat;
        type = String(data: payload.type, encoding: .utf8)
    }
    
    open override var description: String {
        get {
            return "type name format: \(typeNameFormat.name) \ntype: \(type ?? "")"
        }
    }
    
}

open class NdefTextPayload : NdefPayload {
    open var isUtf16Encoding: Bool = false
    open var languageCode: String? = nil
    open var text: String? = nil
    
    open override var description: String {
        get {
            return super.description + " \nis Utf16 encoding: \(isUtf16Encoding) \nlanguage code: \(languageCode ?? "") \ntext: \(text ?? "")"
        }
    }
    
}

open class NdefURIPayload : NdefPayload {
    open var URIString: String? = nil
    
    open override var description: String {
        get {
            return super.description + " \nURI: \(URIString ?? "")"
        }
    }
    
}

open class NdefSmartPosterPayload : NdefPayload {
    open var messagePayloads: [NdefPayload] = []
    
    open override var description: String {
        get {
            return super.description + " \nmessage payloads: [\n\(messagePayloads.map { "\($0)" }.joined(separator:"\n"))\n]"
        }
    }
    
}

open class NdefExternalTypePayload : NdefPayload {
    open var data: Data? = nil {
        didSet {
            if data != nil {
                text = String(data: data!, encoding: .utf8)
            }
            else {
                text = nil
            }
        }
    }
    open var text: String? = nil
    
    open override var description: String {
        get {
            let dataText = data?.map { String(format: "%02x", $0) }.joined(separator: "")
            return super.description + " \ndata: <\(String(describing: dataText))>, \(data?.count ?? 0)bytes \ntext: \(text ?? "")"
        }
    }
    
}

open class NdefTextXVCardPayload : NdefPayload {
    open var text: String? = nil
    
    open override var description: String {
        get {
            return super.description + " \ntext: \(text ?? "")"
        }
    }
    
}

public enum WifiSimpleConfigAuthType : UInt8 {
    case open = 0x00
    case wpaPersonal = 0x01
    case shared = 0x02
    case wpaEnterprise = 0x03
    case wpa2Enterprise = 0x04
    case wpa2Personal = 0x05
    case wpaWpa2Personal = 0x06
    
    var name: String {
        get {
            switch self {
            case .open:
                return "Open"
            case .wpaPersonal:
                return "WPA Personal"
            case .shared:
                return "Shared"
            case .wpaEnterprise:
                return "WPA Enterprise"
            case .wpa2Enterprise:
                return "WPA2 Enterprise"
            case .wpa2Personal:
                return "WPA2 Personal"
            case .wpaWpa2Personal:
                return "WPA/WPA2 Personal"
            }
        }
    }
    
}

public enum WifiSimpleConfigEncryptType : UInt8 {
    case none = 0x00
    case wep = 0x01
    case tkip = 0x02
    case aes = 0x03
    case aesTkip = 0x04
    
    var name: String {
        get {
            switch self {
            case .none:
                return "None"
            case .wep:
                return "WEP"
            case .tkip:
                return "TKIP"
            case .aes:
                return "AES"
            case .aesTkip:
                return "AES/TKIP"
            }
        }
    }
    
}

open class WifiSimpleConfigVersion2 : NSObject {
    open var version: String? = nil
    
    open override var description: String {
        get {
            return "version: \(version ?? "")"
        }
    }
    
}

open class WifiSimpleConfigCredential : NSObject {
    open var ssid: String? = nil
    open var macAddress: String? = nil // "ff:ff:ff:ff:ff:ff" means unlimited
    open var networkIndex: UInt8 = 0
    open var networkKey: String? = nil
    open var authType: WifiSimpleConfigAuthType = .open
    open var encryptType: WifiSimpleConfigEncryptType = .none
    
    open override var description: String {
        get {
            return "ssid: \(ssid ?? "") \nmac address: \(macAddress ?? "") \nnetwork index: \(networkIndex) \npassword: \(networkKey ?? "") \nauthentication type: \(authType.name) \nencryption type: \(encryptType.name)"
        }
    }
    
}

open class NdefWifiSimpleConfigPayload : NdefPayload {
    open var credentials: [WifiSimpleConfigCredential] = []
    open var version2: WifiSimpleConfigVersion2? = nil
    
    open override var description: String {
        get {
            return super.description + " \nversion2: \(String(describing: version2)) \ncredentials: [\n\(credentials.map { "\($0)" }.joined(separator:"\n"))\n]"
        }
    }
    
}


public struct NdefBluetoothMajorServiceClass : OptionSet {
    public var rawValue: Int
    
    public init(rawValue: NdefBluetoothMajorServiceClass.RawValue) {
        self.rawValue = rawValue
    }
    
    public typealias RawValue = Int
    
    public static let information = NdefBluetoothMajorServiceClass(rawValue: 0x01)
    public static let telephony = NdefBluetoothMajorServiceClass(rawValue: 0x02)
    public static let audio = NdefBluetoothMajorServiceClass(rawValue: 0x04)
    public static let objectTransfer = NdefBluetoothMajorServiceClass(rawValue: 0x08)
    public static let capturing = NdefBluetoothMajorServiceClass(rawValue: 0x10)
    public static let rendering = NdefBluetoothMajorServiceClass(rawValue: 0x20)
    public static let networking = NdefBluetoothMajorServiceClass(rawValue: 0x40)
    public static let positioning = NdefBluetoothMajorServiceClass(rawValue: 0x80)
    public static let limitedDiscoverableMode = NdefBluetoothMajorServiceClass(rawValue: 0x100)
    
    var name: String? {
        get {
            var names: [String] = []
            if self.contains(.information) {
                names.append("Information (WEB-server, WAP-server, ...)")
            }
            if self.contains(.telephony) {
                names.append("Telephony (Cordless telephony, Modem, Headset service, ...)")
            }
            if self.contains(.audio) {
                names.append("Audio (Speaker, Microphone, Headset service, ...)")
            }
            if self.contains(.objectTransfer) {
                names.append("Object Transfer (v-Inbox, v-Folder, ...)")
            }
            if self.contains(.capturing) {
                names.append("Capturing (Scanner, Microphone, ...)")
            }
            if self.contains(.rendering) {
                names.append("Rendering (Printing, Speakers, ...)")
            }
            if self.contains(.networking) {
                names.append("Networking (LAN, Ad hoc, ...)")
            }
            if self.contains(.positioning) {
                names.append("Positioning (Location identification)")
            }
            if self.contains(.limitedDiscoverableMode) {
                names.append("Limited Discoverable Mode [Ref #1]")
            }
            
            return names.joined(separator: " ,")
        }
    }
    
}

public enum NdefBluetoothMajorDeviceClass : UInt {
    case miscellaneous = 0
    case computer = 1
    case phone = 2
    case lan_networkAccessPoint = 3
    case audio_video = 4
    case peripheral = 5
    case imaging = 6
    case wearable = 7
    case toy = 8
    case health = 9
    case uncategorized = 31
    case others
    
    var name: String {
        get {
            switch self {
            case .miscellaneous:
                return "Miscellaneous [Ref #2]"
            case .computer:
                return "Computer (desktop, notebook, PDA, organizer, ...)"
            case .phone:
                return "Phone (cellular, cordless, pay phone, modem, ...)"
            case .lan_networkAccessPoint:
                return "LAN/Network Access point"
            case .audio_video:
                return "Audio/Video (headset, speaker, stereo, video display, VCR, ...)"
            case .peripheral:
                return "Peripheral (mouse, joystick, keyboard, ...)"
            case .imaging:
                return "Imaging (printer, scanner, camera, display, ...)"
            case .wearable:
                return "Wearable"
            case .toy:
                return "Toy"
            case .health:
                return "Health"
            case .uncategorized:
                return "Uncategorized: deivce code not specified"
            case .others:
                return "All other values reserved"
            }
        }
    }
}

public protocol NdefBluetoothMinorDeviceClass {
    var name: String { get }
}

public enum ComputerMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case uncategorized
    case others
    case reserved
    
    case desktopWorkstation
    case serverClassComputer
    case laptop
    case handheld_pc_pda
    case palmSize_pc_pda
    case wearableComputer
    case tablet
    
    public var name: String {
        get {
            switch self {
            case .uncategorized:
                return "Uncategorized, code for device not assigned"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .desktopWorkstation:
                return "Desktop workstation"
            case .serverClassComputer:
                return "Server-class computer"
            case .laptop:
                return "Laptop"
            case .handheld_pc_pda:
                return "Handheld PC/PDA (clamshell)"
            case .palmSize_pc_pda:
                return "Palm-size PC/PDA"
            case .wearableComputer:
                return "Wearable computer (watch size)"
            case .tablet:
                return "Tablet"
            }
        }
    }
}

public enum PhoneMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case uncategorized
    case others
    case reserved
    
    case cellular
    case cordless
    case smartphone
    case wiredModemOrVoiceGateway
    case commonIsdnAccess
    
    public var name: String {
        get {
            switch self {
            case .uncategorized:
                return "Uncategorized, code for device not assigned"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .cellular:
                return "Cellular"
            case .cordless:
                return "Cordless"
            case .smartphone:
                return "Smartphone"
            case .wiredModemOrVoiceGateway:
                return "Wired modem or voice gateway"
            case .commonIsdnAccess:
                return "Common ISDN access"
            }
        }
    }
}

public enum LanMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case uncategorized
    case others
    case reserved
    
    case fullAvailable
    /// 1% to 17% utilized
    case utilized_1_to_17
    /// 17% to 33% utilized
    case utilized_17_to_33
    /// 33% to 50% utilized
    case utilized_33_to_50
    /// 50% to 67% utilized
    case utilized_50_to_67
    /// 67% to 83% utilized
    case utilized_67_to_83
    /// 83% to 99% utilized
    case utilized_83_to_99
    case noServiceAvailable
    
    public var name: String {
        get {
            switch self {
            case .uncategorized:
                return "Uncategorized, code for device not assigned"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .fullAvailable:
                return "Fully available"
            case .utilized_1_to_17:
                return "1% to 17% utilized"
            case .utilized_17_to_33:
                return "17% to 33% utilized"
            case .utilized_33_to_50:
                return "33% to 50% utilized"
            case .utilized_50_to_67:
                return "50% to 67% utilized"
            case .utilized_67_to_83:
                return "67% to 83% utilized"
            case .utilized_83_to_99:
                return "83% to 99% utilized"
            case .noServiceAvailable:
                return "No service available"
            }
        }
    }
}

public enum AudioVideoMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case uncategorized
    case others
    case reserved
    
    case wearableHeadsetDevice
    case handsFreeDevice
    case microphone
    case loudspeaker
    case headphones
    case portableAudio
    case carAudio
    case setTopBox
    case HiFiAudioDevice
    case vcr
    case videoCamera
    case camcorder
    case videoMonitor
    case videoDisplayAndLoudspeaker
    case videoConferencing
    case gaming_toy
    
    public var name: String {
        get {
            switch self {
            case .uncategorized:
                return "Uncategorized, code for device not assigned"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .wearableHeadsetDevice:
                return "Wearable Headset Device"
            case .handsFreeDevice:
                return "Hands-free Device"
            case .microphone:
                return "Microphone"
            case .loudspeaker:
                return "Loudspeaker"
            case .headphones:
                return "Headphones"
            case .portableAudio:
                return "Portable Audio"
            case .carAudio:
                return "Car audio"
            case .setTopBox:
                return "Set-top box"
            case .HiFiAudioDevice:
                return "HiFi Audio Device"
            case .vcr:
                return "VCR"
            case .videoCamera:
                return "Video Camera"
            case .camcorder:
                return "Camcorder"
            case .videoMonitor:
                return "Video Monitor"
            case .videoDisplayAndLoudspeaker:
                return "Video Display and Loudspeaker"
            case .videoConferencing:
                return "Video Conferencing"
            case .gaming_toy:
                return "Gaming/Toy"
            }
        }
    }
}

public struct PeripheralMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    public enum KeyboardPointingDevice {
        /// Not keyboard / Not pointing device
        case none
        /// Keyboard
        case keyboard
        /// Pointing device
        case pointingDevice
        /// Combo keyboard/pointing device
        case combo
        
        public var name: String {
            get {
                switch self {
                case .none:
                    return "Not keyboard / Not pointing device"
                case .keyboard:
                    return "Keyboard"
                case .pointingDevice:
                    return "Pointing device"
                case .combo:
                    return "Combo keyboard/pointing device"
                }
            }
        }
    }
    public var keyboardPointingDevice: KeyboardPointingDevice = .none
    
    public enum DeviceType {
        case uncategorized
        case others
        
        case joystick
        case gamepad
        case remoteControl
        case sensingDevice
        case digitizerTablet
        case cardReader
        case digitalPen
        case handheldScanner
        case handheldGesturalInputDevice
        
        public var name: String {
            get {
                switch self {
                case .uncategorized:
                    return "Uncategorized device"
                case .others:
                    return "All other values reserved"
                case .joystick:
                    return "Joystick"
                case .gamepad:
                    return "Gamepad"
                case .remoteControl:
                    return "Remote control"
                case .sensingDevice:
                    return "Sensing device"
                case .digitizerTablet:
                    return "Digitizer tablet"
                case .cardReader:
                    return "Card Reader (e.g. SIM Card Reader)"
                case .digitalPen:
                    return "Digital Pen"
                case .handheldScanner:
                    return "Handheld scanner for bar-codes, RFID, etc."
                case .handheldGesturalInputDevice:
                    return "Handheld gestural input device (e.g., \"wand\" form factor)"
                }
            }
        }
    }
    public var deviceType: DeviceType = .others
    
    public var name: String {
        get {
            return "keyboardPointingDevice: \(keyboardPointingDevice.name) \ndeviceType: \(deviceType.name)"
        }
    }
    
}

public struct ImagingMinorDeviceClass : OptionSet, NdefBluetoothMinorDeviceClass {
    public var rawValue: UInt
    
    public init(rawValue: ImagingMinorDeviceClass.RawValue) {
        self.rawValue = rawValue
    }
    
    public typealias RawValue = UInt
    
    public static let display = ImagingMinorDeviceClass(rawValue: 0x01)
    public static let camera = ImagingMinorDeviceClass(rawValue: 0x02)
    public static let scanner = ImagingMinorDeviceClass(rawValue: 0x04)
    public static let printer = ImagingMinorDeviceClass(rawValue: 0x08)
    
    public static let others = ImagingMinorDeviceClass(rawValue: 0x00)
    
    public var name: String {
        get {
            var names: [String] = []
            if self.contains(.display) {
                names.append("Display")
            }
            if self.contains(.camera) {
                names.append("Camera")
            }
            if self.contains(.scanner) {
                names.append("Scanner")
            }
            if self.contains(.printer) {
                names.append("Printer")
            }
            
            guard names.count > 0 else {
                return "All other values reserved"
            }
            
            return names.joined(separator: " ,")
        }
    }
    
}

public enum WearableMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case uncategorized
    case others
    case reserved
    
    case wristwatch
    case pager
    case jacket
    case helmet
    case glasses
    
    public var name: String {
        get {
            switch self {
            case .uncategorized:
                return "Uncategorized, code for device not assigned"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .wristwatch:
                return "Wristwatch"
            case .pager:
                return "Pager"
            case .jacket:
                return "Jacket"
            case .helmet:
                return "Helmet"
            case .glasses:
                return "Glasses"
            }
        }
    }
}

public enum ToyMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case uncategorized
    case others
    case reserved
    
    case robot
    case vehicle
    case doll_actionFigure
    case controller
    case game
    
    public var name: String {
        get {
            switch self {
            case .uncategorized:
                return "Uncategorized, code for device not assigned"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .robot:
                return "Robot"
            case .vehicle:
                return "Vehicle"
            case .doll_actionFigure:
                return "Doll / Action figure"
            case .controller:
                return "Controller"
            case .game:
                return "Game"
            }
        }
    }
}

public enum HealthMinorDeviceClass : NdefBluetoothMinorDeviceClass {
    case undefined
    case others
    case reserved
    
    case bloodPressureMonitor
    case thermometer
    case weighingScale
    case glucoseMeter
    case pulseOximeter
    case heart_pulseRateMonitor
    case healthDataDisplay
    case stepCounter
    case bodyCompositionAnalyzer
    case peakFlowMonitor
    case medicationMonitor
    case kneeProsthesis
    case ankleProsthesis
    case genericHealthManager
    case personalMobilityDevice
    
    public var name: String {
        get {
            switch self {
            case .undefined:
                return "Undefined"
            case .others:
                return "All other values reserved"
            case .reserved:
                return "(Reserved)"
            case .bloodPressureMonitor:
                return "Blood Pressure Monitor"
            case .thermometer:
                return "Thermometer"
            case .weighingScale:
                return "Weighing Scale"
            case .glucoseMeter:
                return "Glucose Meter"
            case .pulseOximeter:
                return "Pulse Oximeter"
            case .heart_pulseRateMonitor:
                return "Heart/Pulse Rate Monitor"
            case .healthDataDisplay:
                return "Health Data Display"
            case .stepCounter:
                return "Step Counter"
            case .bodyCompositionAnalyzer:
                return "Body Composition Analyzer"
            case .peakFlowMonitor:
                return "Peak Flow Monitor"
            case .medicationMonitor:
                return "Medication Monitor"
            case .kneeProsthesis:
                return "Knee Prosthesis"
            case .ankleProsthesis:
                return "Ankle Prosthesis"
            case .genericHealthManager:
                return "Generic Health Manager"
            case .personalMobilityDevice:
                return "Personal Mobility Device"
            }
        }
    }
}

open class NdefBluetoothDeviceConfigPayload : NdefPayload {
    open var oobDataLength: UInt = 0
    open var macAddress: String? = nil
    open var localName: String? = nil
    
    open var serviceClass: NdefBluetoothMajorServiceClass = []
    open var majorDeviceClass: NdefBluetoothMajorDeviceClass = .others
    open var minorDeviceClass: NdefBluetoothMinorDeviceClass? = nil
    
    open var serviceUuidList_16bits: [String] = []
    
    open override var description: String {
        get {
            return super.description + " \noobDataLength: \(oobDataLength) \nlocalName: \(localName ?? "") \nmacAddress: \(macAddress ?? "") \nservice class: \(String(describing: serviceClass.name)) \nmajor device: \(majorDeviceClass.name) \nminor device: \(String(describing: minorDeviceClass?.name)) \n16-bit Service Class UUID list (complete):\n\(serviceUuidList_16bits)"
        }
    }
    
}
