//
//  ViewController+Parse.swift
//  TestCoreNFC
//
//  Created by realtouchapp on 2019/3/18.
//  Copyright © 2019年 realtouchapp. All rights reserved.
//

import UIKit
import CoreNFC

extension ViewController {
    
    func parsePayload(_ payload: NFCNDEFPayload) -> NdefPayload? {
        var payloadType : String? = nil
        if payload.type.count > 0 {
            payloadType = String(data: payload.type, encoding: .utf8)
        }
        
        let payloadData = payload.payload
        
        var result: NdefPayload? = nil
        
        switch payload.typeNameFormat {
        case .nfcWellKnown:
            
            switch payloadType {
            case "T":
                result = NdefTextPayload(payload)
                
                let parseResult = parseTextPayload(payloadData)
                (result as! NdefTextPayload).isUtf16Encoding = parseResult.0
                (result as! NdefTextPayload).languageCode = parseResult.1
                (result as! NdefTextPayload).text = parseResult.2
                
            case "U":
                result = NdefURIPayload(payload)
                
                let parseResult = parseURIPayload(payloadData)
                (result as! NdefURIPayload).URIString = parseResult
                
            case "Sp":
                result = NdefSmartPosterPayload(payload)
                
                let parseResult = parseSmartPosterPayload(payloadData)
                (result as! NdefSmartPosterPayload).messagePayloads = parseResult
                
            case .none, .some(_):
                fallthrough
            default:
                break
            }
            
        case .absoluteURI:
            result = NdefURIPayload(payload)
            
            (result as! NdefURIPayload).URIString = String(data: payload.payload, encoding: .utf8)
            
        case .media:
            
            switch payloadType {
            case "application/vnd.wfa.wsc":
                result = parseWifiConfigPayload(payload.payload)
                guard result != nil else {
                    break
                }
                
                result!.setPayload(payload)
                
            case "text/vcard":
                result = NdefTextXVCardPayload(payload)
                
                (result as! NdefTextXVCardPayload).text = parseTextXVCardPayload(payload.payload)
                
            case "application/vnd.bluetooth.ep.oob":
                result = parseBluetoothConfigPayload(payload.payload)
                guard result != nil else {
                    break
                }
                
                result!.setPayload(payload)
                
                break
                
            case .none, .some(_):
                fallthrough
            default:
                break
            }
            
            break
        case .nfcExternal:
            result = NdefExternalTypePayload(payload)
            
            (result as! NdefExternalTypePayload).data = payload.payload
            
        case .unknown, .unchanged:
            fallthrough
        default:
            break
        }
        
        return result
    }
    
    /// 解析純文字格式
    fileprivate func parseTextPayload(_ payloadData : Data) -> (Bool, String, String) {
        guard payloadData.count >= 1 else {
            return (false, "", "")
        }
        
        let checkByte = payloadData[0]
        let isUtf16 = ((checkByte & 0x80) > 0) as Bool
        let langCodeLength = (checkByte & 0x7F)
        
        guard (langCodeLength + 1) <= payloadData.count else {
            return (false, "", "")
        }
        
        let langCode = String(data: payloadData[1..<(langCodeLength + 1)], encoding: (isUtf16 ? .utf16 : .utf8))
        let text = String(data: payloadData[(langCodeLength + 1)...], encoding: (isUtf16 ? .utf16 : .utf8))
        
        return (isUtf16, langCode ?? "", text ?? "")
    }
    
    /// 解析URI格式
    fileprivate func parseURIPayload(_ payloadData : Data) -> String {
        guard payloadData.count >= 1 else {
            return ""
        }
        
        guard let originalText = String(data: payloadData[1...], encoding: .utf8) else {
            return ""
        }
        
        let code = payloadData[0]
        var text = ""
        switch code {
        case 0x00: // N/A. No prepending is done
            text = originalText;
        case 0x01: // http://www.
            text = "http://www.".appending(originalText)
        case 0x02: // https://www.
            text = "https://www.".appending(originalText)
        case 0x03: // http://
            text = "http://".appending(originalText)
        case 0x04: // https://
            text = "https://".appending(originalText)
        case 0x05: // tel:
            text = "tel:".appending(originalText)
        case 0x06: // mailto:
            text = "mailto:".appending(originalText)
        case 0x07: // ftp://anonymous:anonymous@
            text = "ftp://anonymous:anonymous@".appending(originalText)
        case 0x08: // ftp://ftp.
            text = "ftp://ftp.".appending(originalText)
        case 0x09: // ftps://
            text = "ftps://".appending(originalText)
        case 0x0A: // sftp://
            text = "sftp://".appending(originalText)
        case 0x0B: // smb://
            text = "smb://".appending(originalText)
        case 0x0C: // nfs://
            text = "nfs://".appending(originalText)
        case 0x0D: // ftp://
            text = "ftp://".appending(originalText)
        case 0x0E: // dav://
            text = "dav://".appending(originalText)
        case 0x0F: // news:
            text = "news:".appending(originalText)
        case 0x10: // telnet://
            text = "telnet://".appending(originalText)
        case 0x11: // imap:
            text = "imap:".appending(originalText)
        case 0x12: // rtsp://
            text = "rtsp://".appending(originalText)
        case 0x13: // urn:
            text = "urn:".appending(originalText)
        case 0x14: // pop:
            text = "pop:".appending(originalText)
        case 0x15: // sip:
            text = "sip:".appending(originalText)
        case 0x16: // sips:
            text = "sips:".appending(originalText)
        case 0x17: // tftp:
            text = "tftp:".appending(originalText)
        case 0x18: // btspp://
            text = "btspp://".appending(originalText)
        case 0x19: // btl2cap://
            text = "btl2cap://".appending(originalText)
        case 0x1A: // btgoep://
            text = "btgoep://".appending(originalText)
        case 0x1B: // tcpobex://
            text = "tcpobex://".appending(originalText)
        case 0x1C: // irdaobex://
            text = "irdaobex://".appending(originalText)
        case 0x1D: // file://
            text = "file://".appending(originalText)
        case 0x1E: // urn:epc:id:
            text = "urn:epc:id:".appending(originalText)
        case 0x1F: // urn:epc:tag:
            text = "urn:epc:tag:".appending(originalText)
        case 0x20: // urn:epc:pat:
            text = "urn:epc:pat:".appending(originalText)
        case 0x21: // urn:epc:raw:
            text = "urn:epc:raw:".appending(originalText)
        case 0x22: // urn:epc:
            text = "urn:epc:".appending(originalText)
        case 0x23: // urn:nfc:
            text = "urn:nfc:".appending(originalText)
        default: // 0x24-0xFF RFU Reserved for Future Use, Not Valid Inputs
            break;
        }
        
        return text
    }
    
    fileprivate func parseSmartPosterPayload(_ payloadData : Data) -> [NdefPayload] {
        guard payloadData.count >= 1 else {
            return []
        }
        
        var payloadResults: [NdefPayload] = []
        
        var data: Data = payloadData
        var header: NdefSpHeader? = parseSpHeader(data)
        while header != nil {
            data = Data(data[header!.payloadOffset...])
            
            switch header!.type {
            case "T":
                
                let result = NdefTextPayload()
                result.type = header!.type
                switch header!.typeNameFormat {
                case .NFC_Well_Known_Type:
                    result.typeNameFormat = .nfcWellKnown
                case .Empty:
                    result.typeNameFormat = .empty
                case .Media_Type:
                    result.typeNameFormat = .media
                case .Absolute_URI:
                    result.typeNameFormat = .absoluteURI
                case .External_Type:
                    result.typeNameFormat = .nfcExternal
                case .UnKnown:
                    result.typeNameFormat = .unknown
                case .UnChanged:
                    result.typeNameFormat = .unchanged
                case .Reserved:
                    result.typeNameFormat = .empty
                }
                
                let parseResult = parseTextPayload(Data(data[..<header!.payloadLength]))
                result.isUtf16Encoding = parseResult.0
                result.languageCode = parseResult.1
                result.text = parseResult.2
                
                payloadResults.append(result)
                
            case "U":
                
                let result = NdefURIPayload()
                result.type = header!.type
                switch header!.typeNameFormat {
                case .NFC_Well_Known_Type:
                    result.typeNameFormat = .nfcWellKnown
                case .Empty:
                    result.typeNameFormat = .empty
                case .Media_Type:
                    result.typeNameFormat = .media
                case .Absolute_URI:
                    result.typeNameFormat = .absoluteURI
                case .External_Type:
                    result.typeNameFormat = .nfcExternal
                case .UnKnown:
                    result.typeNameFormat = .unknown
                case .UnChanged:
                    result.typeNameFormat = .unchanged
                case .Reserved:
                    result.typeNameFormat = .empty
                }
                
                let parseResult = parseURIPayload(Data(data[..<header!.payloadLength]))
                result.URIString = parseResult
                
                payloadResults.append(result)
                
            default:
                // Currently other records are not supported.
                return []
            }
            
            data = Data(data[header!.payloadLength...])
            if header!.messageEnd || data.count == 0 {
                break
            }
            
            header = parseSpHeader(data)
        }
        
        guard payloadResults.count > 0 else {
            return []
        }
        
        print("payloadResults = \(payloadResults)")
        
        return payloadResults
    }
    
    fileprivate struct NdefSpHeader {
        
        enum TypeNameFormatCode: UInt8 {
            case Empty = 0x00
            case NFC_Well_Known_Type = 0x01
            case Media_Type = 0x02
            case Absolute_URI = 0x03
            case External_Type = 0x04
            case UnKnown = 0x05
            case UnChanged = 0x06
            case Reserved = 0x07
        }
        
        var messageBegin: Bool = false
        var messageEnd: Bool = false
        var chunkFlag: Bool = false
        var shortRecord: Bool = false
        var idLengthPresent: Bool = false
        var typeNameFormat: TypeNameFormatCode = .Empty
        
        var typeLength: UInt = 0
        var payloadLength: UInt = 0
        var idLength: UInt = 0
        var type: String? = nil
        var ID: String? = nil
        var payload: Data? = nil
        
        var payloadOffset: UInt = 0
        
        init() { }
        
    }
    
    fileprivate func parseSpHeader(_ data : Data) -> NdefSpHeader? {
        guard data.count > 0 else {
            return nil
        }
        
//        print("Data = \(data)")
//        for c in data {
//            print("\(String(format: "0x%02X", c))")
//        }
        
        var header = NdefSpHeader()
        
        let statusByte = data[0]
        //        print("statusByte = \(String(statusByte, radix: 2))")
        header.messageBegin = (statusByte & 0x80) > 0
        header.messageEnd = (statusByte & 0x40) > 0
        header.chunkFlag = (statusByte & 0x20) > 0
        header.shortRecord = (statusByte & 0x10) > 0
        header.idLengthPresent = (statusByte & 0x08) > 0
        header.typeNameFormat = NdefSpHeader.TypeNameFormatCode(rawValue: (statusByte & 0x07)) ?? .Empty
        
        guard data.count > 1 else {
            return nil
        }
        
        header.typeLength = UInt(data[1])
        
        var index: UInt = 2
        
        guard (header.shortRecord && data.count > 2) || (!header.shortRecord && data.count > 6) else {
            return nil
        }
        
        if header.shortRecord {
            header.payloadLength = UInt(data[Int(index)])
            index += 1
            
        }
        else {
            let tmp = data[index..<(index+4)].reversed()
            var s: UInt32 = 0
            NSData(data: Data(tmp)).getBytes(&s, length: 4)
            
            header.payloadLength = UInt(s)
            index += 4
        }
        
        header.idLength = 0
        if header.idLengthPresent {
            guard index + 1 <= data.count else {
                return nil
            }
            
            header.idLength = UInt(data[Int(index)])
            index += 1
        }
        
        guard index + header.typeLength <= data.count else {
            return nil
        }
        
        header.type = String(data: data[index..<(index+header.typeLength)], encoding: .utf8)
        index += header.typeLength
        
        if header.idLength > 0 {
            guard index + header.idLength <= data.count else {
                return nil
            }
            
            header.ID = String(data: data[index..<(index+header.idLength)], encoding: .utf8)
            index += header.idLength
        }
        
        header.payloadOffset = index;
        
        return header
    }
    
    fileprivate func parseTextXVCardPayload(_ payloadData : Data) -> String? {
        guard let text = String(data: payloadData, encoding: .utf8) else {
            return nil
        }
        
        return text
    }
    
    fileprivate func parseWifiConfigPayload(_ payloadData : Data) -> NdefWifiSimpleConfigPayload? {
        guard payloadData.count >= 2 else {
            return nil
        }
        
//        print("Data = \(payloadData)")
//        for c in payloadData {
//            print("\(String(format: "0x%02X", c))")
//        }
        
        let payload = NdefWifiSimpleConfigPayload()
        
        var index = 0
        while index <= payloadData.count - 2 {
            if payloadData[index] == WifiVariables.VENDOR_EXT.0 && payloadData[index + 1] == WifiVariables.VENDOR_EXT.1 {
                index += 2
                guard index + 2 <= payloadData.count else {
                    return nil
                }
                
                let ext_length = uint16FromBigEndian(payloadData[index..<(index+2)].map { $0 })
                index += 2
                let data = Data(payloadData[index..<(index+Int(ext_length))])
                payload.version2 = parseWifiSimpleConfigVersion2(data)
                index += Int(ext_length)
                
            }
            else if payloadData[index] == WifiVariables.CREDENTIAL.0 && payloadData[index + 1] == WifiVariables.CREDENTIAL.1 {
                index += 2
                let credential_length = uint16FromBigEndian(payloadData[index..<(index+2)].map { $0 })
                index += 2
                
                let data = Data(payloadData[index..<(index+Int(credential_length))])
                guard let credential = parseWifiSimpleConfigCredential(data) else {
                    return nil
                }
                payload.credentials.append(credential)
                index += Int(credential_length);
                
            }
            else {
                break
            }
        }
        
        return payload
    }
    
    fileprivate class WifiVariables {
        static let CREDENTIAL = (0x10, 0x0e)
        static let SSID = (0x10, 0x45)
        static let MAC_ADDRESS = (0x10, 0x20)
        static let NETWORK_INDEX = (0x10, 0x26)
        static let NETWORK_KEY = (0x10, 0x27)
        static let AUTH_TYPE = (0x10, 0x03)
        static let ENCRYPT_TYPE = (0x10, 0x0f)
        static let VENDOR_EXT = (0x10, 0x49)
        
        static let VENDOR_ID_WFA = (0x00, 0x37, 0x2a)
        static let WFA_VERSION2 = (0x00)
    
        static let AUTH_OPEN = (0x00, 0x01)
        static let AUTH_WPA_PERSONAL = (0x00, 0x02)
        static let AUTH_SHARED = (0x00, 0x04)
        static let AUTH_WPA_ENTERPRISE = (0x00, 0x08)
        static let AUTH_WPA2_ENTERPRISE = (0x00, 0x10)
        static let AUTH_WPA2_PERSONAL = (0x00, 0x20)
        static let AUTH_WPA_WPA2_PERSONAL = (0x00, 0x22)
        
        static let ENCRYPT_NONE = (0x00, 0x01)
        static let ENCRYPT_WEP = (0x00, 0x02)
        static let ENCRYPT_TKIP = (0x00, 0x04)
        static let ENCRYPT_AES = (0x00, 0x08)
        static let ENCRYPT_AES_TKIP = (0x00, 0x0c)
    }
    
    fileprivate func uint16FromBigEndian(_ p : [UInt8]) -> UInt16 {
        let tmp = p.reversed()
        var s: UInt16 = 0
        NSData(data: Data(tmp)).getBytes(&s, length: 2)
        
        return s
    }
    
    fileprivate func parseWifiSimpleConfigCredential(_ data : Data) -> WifiSimpleConfigCredential? {
        guard data.count >= 2 else {
            return nil
        }
        
        let credential = WifiSimpleConfigCredential()
        
        var index = 0
        while index <= data.count - 2 {
            if data[index] == WifiVariables.SSID.0 && data[index + 1] == WifiVariables.SSID.1 {
                // Parse SSID
                index += 2
                let sublength = uint16FromBigEndian(data[index..<(index+2)].map { $0 })
                index += 2
                guard let ssid = String(data: data[index..<(index+Int(sublength))], encoding: .utf8) else {
                    return nil
                }
                credential.ssid = ssid
                index += Int(sublength)
                
            }
            else if data[index] == WifiVariables.MAC_ADDRESS.0 && data[index + 1] == WifiVariables.MAC_ADDRESS.1 {
                // Parse MAC address
                index += 2
                index += 2 // Skip length
                credential.macAddress = String(format: "%02x:%02x:%02x:%02x:%02x:%02x", data[index], data[index + 1], data[index + 2], data[index + 3], data[index + 4], data[index + 5])
                index += 6
                
            }
            else if data[index] == WifiVariables.NETWORK_INDEX.0 && data[index + 1] == WifiVariables.NETWORK_INDEX.1 {
                // Parse network index (there could be more than one network).
                index += 2
                credential.networkIndex = data[index]
                index += 1
                
            }
            else if data[index] == WifiVariables.NETWORK_KEY.0 && data[index + 1] == WifiVariables.NETWORK_KEY.1 {
                // Parse network key (password)
                index += 2
                let sublength = uint16FromBigEndian(data[index..<(index+2)].map { $0 })
                index += 2
                guard let password = String(data: data[index..<(index+Int(sublength))], encoding: .utf8) else {
                    return nil
                }
                credential.networkKey = password
                index += Int(sublength)
                
            }
            else if data[index] == WifiVariables.AUTH_TYPE.0 && data[index + 1] == WifiVariables.AUTH_TYPE.1 {
                // Parse authentication type
                index += 2
                index += 2 // Skip length
                var type = WifiSimpleConfigAuthType.open
                if data[index] == WifiVariables.AUTH_OPEN.0 && data[index + 1] == WifiVariables.AUTH_OPEN.1 {
                    type = .open
                }
                else if data[index] == WifiVariables.AUTH_WPA_PERSONAL.0 && data[index + 1] == WifiVariables.AUTH_WPA_PERSONAL.1 {
                    type = .wpaPersonal
                }
                else if data[index] == WifiVariables.AUTH_SHARED.0 && data[index + 1] == WifiVariables.AUTH_SHARED.1 {
                    type = .shared
                }
                else if data[index] == WifiVariables.AUTH_WPA_ENTERPRISE.0 && data[index + 1] == WifiVariables.AUTH_WPA_ENTERPRISE.1 {
                    type = .wpaEnterprise
                }
                else if data[index] == WifiVariables.AUTH_WPA2_ENTERPRISE.0 && data[index + 1] == WifiVariables.AUTH_WPA2_ENTERPRISE.1 {
                    type = .wpa2Enterprise
                }
                else if data[index] == WifiVariables.AUTH_WPA2_PERSONAL.0 && data[index + 1] == WifiVariables.AUTH_WPA2_PERSONAL.1 {
                    type = .wpa2Personal
                }
                else if data[index] == WifiVariables.AUTH_WPA_WPA2_PERSONAL.0 && data[index + 1] == WifiVariables.AUTH_WPA_WPA2_PERSONAL.1 {
                    type = .wpaWpa2Personal
                }
                else {
                    // Unhandled auth type.
                }
                credential.authType = type
                index += 2
                
            }
            else if data[index] == WifiVariables.ENCRYPT_TYPE.0 && data[index + 1] == WifiVariables.ENCRYPT_TYPE.1 {
                // Parse encryption type
                index += 2;
                index += 2; // Skip length
                var type = WifiSimpleConfigEncryptType.none
                if data[index] == WifiVariables.ENCRYPT_NONE.0 && data[index + 1] == WifiVariables.ENCRYPT_NONE.1 {
                    type = .none
                }
                else if data[index] == WifiVariables.ENCRYPT_WEP.0 && data[index + 1] == WifiVariables.ENCRYPT_WEP.1 {
                    type = .wep
                }
                else if data[index] == WifiVariables.ENCRYPT_TKIP.0 && data[index + 1] == WifiVariables.ENCRYPT_TKIP.1 {
                    type = .tkip
                }
                else if data[index] == WifiVariables.ENCRYPT_AES.0 && data[index + 1] == WifiVariables.ENCRYPT_AES.1 {
                    type = .aes
                }
                else if data[index] == WifiVariables.ENCRYPT_AES_TKIP.0 && data[index + 1] == WifiVariables.ENCRYPT_AES_TKIP.1 {
                    type = .aesTkip
                }
                else {
                    // Unhandled encryption type.
                }
                credential.encryptType = type
                index += 2
                
            }
            else { // Unknown attribute
                // In "Wi-Fi Simple Configuration Technical Specification v2.0.4", page 100 (Configuration Token
                // - Credential - Data Element Definitions), it says "Note: Unrecognized attributes in messages
                // shall beignored; they shall not cause the message to be rejected."
                // Currently we found unknown attribute: 0x0101
                index += 2;
            }
        }
        
        return credential
    }
    
    // ---------------WFA Vendor Extension Subelements-------------------------
    // Description                      ID              Length
    // Version2                         0x00            1B
    // AuthorizedMACs                   0x01            <=30B
    // Network Key Shareable            0x02            Bool
    // Request to Enroll                0x03            Bool
    // Settings Delay Time              0x04            1B
    // Registrar Configuration  Methods 0x05            2B
    // Reserved for future use          0x06 to 0xFF
    
    // Version2 value: 0x20 = version 2.0, 0x21 = version 2.1, etc. Shall be included in protocol version 2.0 and higher.
    // If Version2 does not exist, assume version is "1.0h".
    fileprivate func parseWifiSimpleConfigVersion2(_ data : Data) -> WifiSimpleConfigVersion2? {
        var index = 0
        guard index + 3 <= data.count else {
            return nil
        }
        
        let version2 = WifiSimpleConfigVersion2()
        if data[index] == WifiVariables.VENDOR_ID_WFA.0 && data[index + 1] == WifiVariables.VENDOR_ID_WFA.1 && data[index + 2] == WifiVariables.VENDOR_ID_WFA.2 {
            // Parse vendor extension wfa
            index += 3
            guard index + 1 <= data.count else {
                return nil
            }
            
            if data[index] == WifiVariables.WFA_VERSION2 {
                index += 1
                // Parse Version2
                let ver2_length = data[index]
                guard ver2_length == 1 else {
                    return nil
                }
                
                index += 1
                let ver = data[index]
                if ver == 0x20 {
                    version2.version = "2.0"
                }
                else if ver == 0x21 {
                    version2.version = "2.1"
                }
                else {
                    return nil
                }
                
                index += 1
                
            }
            
        }
        
        return version2
    }
    
    fileprivate func parseBluetoothConfigPayload(_ payloadData : Data) -> NdefBluetoothDeviceConfigPayload? {
//        print("Data = \(payloadData)")
//        for c in payloadData {
//            print("\(String(format: "0x%02X", c)), c=\(String(format: "%c", c))")
//        }
        
        guard payloadData.count >= 8 else {
            return nil
        }
        
        let payload = NdefBluetoothDeviceConfigPayload()
        
        var index = 0
        
        let tmp = payloadData[index..<(index+2)]
        var oobDatalength: UInt16 = 0
        NSData(data: Data(tmp)).getBytes(&oobDatalength, length: 2)
        payload.oobDataLength = UInt(oobDatalength)
        
        index += 2
        
        let macAddress = payloadData[index..<(index+6)].map { String(format: "%02X", $0) }.reversed().joined(separator: ":");
        index += 6
        payload.macAddress = macAddress
        
        while index < payloadData.count {
            let dataLength = payloadData[index];
            index += 1
            guard index < payloadData.count else {
                break
            }
            
            let type = payloadData[index];
            index += 1
            guard (index + Int(dataLength) - 1) <= payloadData.count else {
                break
            }
            
            switch type {
            case 0x09, 0x08: // Complete Local Name
                
                let localName = String(data: Data(payloadData[index..<(index+Int(dataLength)-1)]), encoding: .utf8)
                payload.localName = localName
                
            case 0x0d: // Class of Device
                
                let deviceClasses = parseBluetoothDeviceClasses(data: Data(payloadData[index..<(index+Int(dataLength)-1)]))
                payload.serviceClass = deviceClasses.0
                payload.majorDeviceClass = deviceClasses.1
                payload.minorDeviceClass = deviceClasses.2
                
            case 0x03: // 16-bit Service Class UUID list (complete)
                
                // https://www.bluetooth.com/specifications/assigned-numbers/service-discovery
                var uuidList: [String] = []
                let data = Data(payloadData[index..<(index+Int(dataLength)-1)])
                for i in stride(from: 0, to: Int(dataLength)-1, by: 2) {
                    guard i + 1 < Int(dataLength)-1 else {
                        break
                    }
                    
                    let uuid = String(format: "%02X%02X", data[i+1], data[i])
                    uuidList.append(uuid)
                    
                }
                payload.serviceUuidList_16bits = uuidList
                
            default:
                // 其他先算了，改天再慢慢解
                // https://www.bluetooth.com/specifications/assigned-numbers/generic-access-profile
                break
            }
            
            index += (Int(dataLength) - 1)
            
        }
        
        return payload
    }
    
    // https://www.bluetooth.com/specifications/assigned-numbers/baseband
    // return (service class, major class, minor class)
    fileprivate func parseBluetoothDeviceClasses(data : Data) -> (NdefBluetoothMajorServiceClass, NdefBluetoothMajorDeviceClass, NdefBluetoothMinorDeviceClass?) {
        guard data.count > 2 else {
            return ([], .others, nil)
        }
        
        let tmp = data
        var s: UInt32 = 0
        NSData(data: Data(tmp)).getBytes(&s, length: 3)
        
        var majorServiceClass: NdefBluetoothMajorServiceClass = []
        if Int(s) & 0x800000 > 0 {
            majorServiceClass.insert(.information)
        }
        if Int(s) & 0x400000 > 0 {
            majorServiceClass.insert(.telephony)
        }
        if Int(s) & 0x200000 > 0 {
            majorServiceClass.insert(.audio)
        }
        if Int(s) & 0x100000 > 0 {
            majorServiceClass.insert(.objectTransfer)
        }
        if Int(s) & 0x080000 > 0 {
            majorServiceClass.insert(.capturing)
        }
        if Int(s) & 0x040000 > 0 {
            majorServiceClass.insert(.rendering)
        }
        if Int(s) & 0x020000 > 0 {
            majorServiceClass.insert(.networking)
        }
        if Int(s) & 0x010000 > 0 {
            majorServiceClass.insert(.positioning)
        }
        if Int(s) & 0x002000 > 0 {
            majorServiceClass.insert(.limitedDiscoverableMode)
        }
        
        let majorDeviceClassValue = (Int(s) & 0x1f00) >> 8
        var majorDeviceClass = NdefBluetoothMajorDeviceClass.others
        let minorDeviceClassValue = (Int(s) & 0xfc) >> 2
        var minorDeviceClass: NdefBluetoothMinorDeviceClass? = nil
        switch majorDeviceClassValue {
        case 0x00:
            majorDeviceClass = .miscellaneous
        case 0x01:
            majorDeviceClass = .computer
            
            switch minorDeviceClassValue {
            case 0x00:
                minorDeviceClass = ComputerMinorDeviceClass.uncategorized
            case 0x01:
                minorDeviceClass = ComputerMinorDeviceClass.desktopWorkstation
            case 0x02:
                minorDeviceClass = ComputerMinorDeviceClass.serverClassComputer
            case 0x03:
                minorDeviceClass = ComputerMinorDeviceClass.laptop
            case 0x04:
                minorDeviceClass = ComputerMinorDeviceClass.handheld_pc_pda
            case 0x05:
                minorDeviceClass = ComputerMinorDeviceClass.palmSize_pc_pda
            case 0x06:
                minorDeviceClass = ComputerMinorDeviceClass.wearableComputer
            case 0x07:
                minorDeviceClass = ComputerMinorDeviceClass.tablet
            default:
                minorDeviceClass = ComputerMinorDeviceClass.others
            }
            
        case 0x02:
            majorDeviceClass = .phone
            
            switch minorDeviceClassValue {
            case 0x00:
                minorDeviceClass = PhoneMinorDeviceClass.uncategorized
            case 0x01:
                minorDeviceClass = PhoneMinorDeviceClass.cellular
            case 0x02:
                minorDeviceClass = PhoneMinorDeviceClass.cordless
            case 0x03:
                minorDeviceClass = PhoneMinorDeviceClass.smartphone
            case 0x04:
                minorDeviceClass = PhoneMinorDeviceClass.wiredModemOrVoiceGateway
            case 0x05:
                minorDeviceClass = PhoneMinorDeviceClass.commonIsdnAccess
            default:
                minorDeviceClass = PhoneMinorDeviceClass.others
            }
            
        case 0x03:
            majorDeviceClass = .lan_networkAccessPoint
            
            //  bits 2, 1, 0 are reserved.
            switch (minorDeviceClassValue & 0x38) {
            case 0x00:
                minorDeviceClass = LanMinorDeviceClass.fullAvailable
            case 0x01:
                minorDeviceClass = LanMinorDeviceClass.utilized_1_to_17
            case 0x02:
                minorDeviceClass = LanMinorDeviceClass.utilized_17_to_33
            case 0x03:
                minorDeviceClass = LanMinorDeviceClass.utilized_33_to_50
            case 0x04:
                minorDeviceClass = LanMinorDeviceClass.utilized_50_to_67
            case 0x05:
                minorDeviceClass = LanMinorDeviceClass.utilized_67_to_83
            case 0x06:
                minorDeviceClass = LanMinorDeviceClass.utilized_83_to_99
            case 0x07:
                minorDeviceClass = LanMinorDeviceClass.noServiceAvailable
            default:
                minorDeviceClass = LanMinorDeviceClass.others
            }
            
        case 0x04:
            majorDeviceClass = .audio_video
            
            switch minorDeviceClassValue {
            case 0x00:
                minorDeviceClass = AudioVideoMinorDeviceClass.uncategorized
            case 0x01:
                minorDeviceClass = AudioVideoMinorDeviceClass.wearableHeadsetDevice
            case 0x02:
                minorDeviceClass = AudioVideoMinorDeviceClass.handsFreeDevice
            case 0x03, 0x11:
                minorDeviceClass = AudioVideoMinorDeviceClass.reserved
            case 0x04:
                minorDeviceClass = AudioVideoMinorDeviceClass.microphone
            case 0x05:
                minorDeviceClass = AudioVideoMinorDeviceClass.loudspeaker
            case 0x06:
                minorDeviceClass = AudioVideoMinorDeviceClass.headphones
            case 0x07:
                minorDeviceClass = AudioVideoMinorDeviceClass.portableAudio
            case 0x08:
                minorDeviceClass = AudioVideoMinorDeviceClass.carAudio
            case 0x09:
                minorDeviceClass = AudioVideoMinorDeviceClass.setTopBox
            case 0x0a:
                minorDeviceClass = AudioVideoMinorDeviceClass.HiFiAudioDevice
            case 0x0b:
                minorDeviceClass = AudioVideoMinorDeviceClass.vcr
            case 0x0c:
                minorDeviceClass = AudioVideoMinorDeviceClass.videoCamera
            case 0x0d:
                minorDeviceClass = AudioVideoMinorDeviceClass.camcorder
            case 0x0e:
                minorDeviceClass = AudioVideoMinorDeviceClass.videoMonitor
            case 0x0f:
                minorDeviceClass = AudioVideoMinorDeviceClass.videoDisplayAndLoudspeaker
            case 0x10:
                minorDeviceClass = AudioVideoMinorDeviceClass.videoConferencing
            case 0x12:
                minorDeviceClass = AudioVideoMinorDeviceClass.gaming_toy
            default:
                minorDeviceClass = AudioVideoMinorDeviceClass.others
            }
            
        case 0x05:
            majorDeviceClass = .peripheral
            
            minorDeviceClass = PeripheralMinorDeviceClass()
            let firstDeviceClassBits = (minorDeviceClassValue & 0x30) >> 4;
            let secondDeviceClassBits = (minorDeviceClassValue & 0x0f);
            if var tempClass = minorDeviceClass as? PeripheralMinorDeviceClass {
                switch firstDeviceClassBits {
                case 0x00:
                    tempClass.keyboardPointingDevice = .none
                case 0x01:
                    tempClass.keyboardPointingDevice = .keyboard
                case 0x02:
                    tempClass.keyboardPointingDevice = .pointingDevice
                case 0x03:
                    tempClass.keyboardPointingDevice = .combo
                default:
                    tempClass.keyboardPointingDevice = .none
                }
                
                switch secondDeviceClassBits {
                case 0x00:
                    tempClass.deviceType = .uncategorized
                case 0x01:
                    tempClass.deviceType = .joystick
                case 0x02:
                    tempClass.deviceType = .gamepad
                case 0x03:
                    tempClass.deviceType = .remoteControl
                case 0x04:
                    tempClass.deviceType = .sensingDevice
                case 0x05:
                    tempClass.deviceType = .digitizerTablet
                case 0x06:
                    tempClass.deviceType = .cardReader
                case 0x07:
                    tempClass.deviceType = .digitalPen
                case 0x08:
                    tempClass.deviceType = .handheldScanner
                case 0x09:
                    tempClass.deviceType = .handheldGesturalInputDevice
                default:
                    tempClass.deviceType = .others
                }
                
            }
            
        case 0x06:
            majorDeviceClass = .imaging
            
            minorDeviceClass = [] as ImagingMinorDeviceClass
            // bit 1, 0 are reserved.
            let classBits = (minorDeviceClassValue & 0x3c) >> 2
            if var tempClass = minorDeviceClass as? ImagingMinorDeviceClass {
                if (classBits & 0x01) > 0 {
                    tempClass.insert(.display)
                }
                if (classBits & 0x02) > 0 {
                    tempClass.insert(.camera)
                }
                if (classBits & 0x04) > 0 {
                    tempClass.insert(.scanner)
                }
                if (classBits & 0x08) > 0 {
                    tempClass.insert(.printer)
                }
            }
            
        case 0x07:
            majorDeviceClass = .wearable
            
            switch minorDeviceClassValue {
            case 0x01:
                minorDeviceClass = WearableMinorDeviceClass.wristwatch
            case 0x02:
                minorDeviceClass = WearableMinorDeviceClass.pager
            case 0x03:
                minorDeviceClass = WearableMinorDeviceClass.jacket
            case 0x04:
                minorDeviceClass = WearableMinorDeviceClass.helmet
            case 0x05:
                minorDeviceClass = WearableMinorDeviceClass.glasses
            default:
                minorDeviceClass = WearableMinorDeviceClass.others
            }
            
        case 0x08:
            majorDeviceClass = .toy
            
            switch minorDeviceClassValue {
            case 0x01:
                minorDeviceClass = ToyMinorDeviceClass.robot
            case 0x02:
                minorDeviceClass = ToyMinorDeviceClass.vehicle
            case 0x03:
                minorDeviceClass = ToyMinorDeviceClass.doll_actionFigure
            case 0x04:
                minorDeviceClass = ToyMinorDeviceClass.controller
            case 0x05:
                minorDeviceClass = ToyMinorDeviceClass.game
            default:
                minorDeviceClass = ToyMinorDeviceClass.others
            }
            
        case 0x09:
            majorDeviceClass = .health
            
            switch minorDeviceClassValue {
            case 0x00:
                minorDeviceClass = HealthMinorDeviceClass.undefined
            case 0x01:
                minorDeviceClass = HealthMinorDeviceClass.bloodPressureMonitor
            case 0x02:
                minorDeviceClass = HealthMinorDeviceClass.thermometer
            case 0x03:
                minorDeviceClass = HealthMinorDeviceClass.weighingScale
            case 0x04:
                minorDeviceClass = HealthMinorDeviceClass.glucoseMeter
            case 0x05:
                minorDeviceClass = HealthMinorDeviceClass.pulseOximeter
            case 0x06:
                minorDeviceClass = HealthMinorDeviceClass.heart_pulseRateMonitor
            case 0x07:
                minorDeviceClass = HealthMinorDeviceClass.healthDataDisplay
            case 0x08:
                minorDeviceClass = HealthMinorDeviceClass.stepCounter
            case 0x09:
                minorDeviceClass = HealthMinorDeviceClass.bodyCompositionAnalyzer
            case 0x0a:
                minorDeviceClass = HealthMinorDeviceClass.peakFlowMonitor
            case 0x0b:
                minorDeviceClass = HealthMinorDeviceClass.medicationMonitor
            case 0x0c:
                minorDeviceClass = HealthMinorDeviceClass.kneeProsthesis
            case 0x0d:
                minorDeviceClass = HealthMinorDeviceClass.ankleProsthesis
            case 0x0e:
                minorDeviceClass = HealthMinorDeviceClass.genericHealthManager
            case 0x0f:
                minorDeviceClass = HealthMinorDeviceClass.personalMobilityDevice
            default:
                minorDeviceClass = HealthMinorDeviceClass.others
            }
            
        case 0x1f:
            majorDeviceClass = .uncategorized
        default:
            break
        }
        
        print("service class: \(String(describing: majorServiceClass.name))")
        print("major device: \(String(describing: majorDeviceClass.name))")
        if minorDeviceClass != nil {
            print("minor device: \(String(describing: minorDeviceClass!.name))")
        }
        
        return (majorServiceClass, majorDeviceClass, minorDeviceClass)
    }
    
}
