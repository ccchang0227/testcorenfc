//
//  ViewController.swift
//  TestCoreNFC
//
//  Created by realtouchapp on 2019/3/18.
//  Copyright © 2019年 realtouchapp. All rights reserved.
//

import UIKit
import CoreNFC
import SafariServices

class ViewController: UIViewController, NFCNDEFReaderSessionDelegate, SFSafariViewControllerDelegate {

    @IBOutlet weak var scanButton: UIButton!
    
    fileprivate var session: NFCNDEFReaderSession?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.scanButton.layer.cornerRadius = 4
        
    }

    // MARK: -
    
    func showAlert(title: String?, message: String?, actions: UIAlertAction...) -> UIAlertController {
        let alertController = UIAlertController(title: title,
                                                message: message,
                                                preferredStyle: .alert)
        for action in actions {
            alertController.addAction(action)
        }
        
        DispatchQueue.main.async {
            self.present(alertController, animated: true, completion: nil)
        }
        
        return alertController
    }
    
    // MARK: - Actions

    @IBAction func tapScanAction(_ sender: Any) {
        guard NFCNDEFReaderSession.readingAvailable == true else {
            
            _ = self.showAlert(title: "NFC reading is not available on this device.",
                               message: nil,
                               actions: UIAlertAction(title: "OK", style: .default, handler: nil))
            
            return
        }
        
        session = NFCNDEFReaderSession(delegate: self, queue: nil, invalidateAfterFirstRead: true)
        session?.alertMessage = "Hold your iPhone near the item."
        session?.begin()
    }
    
    // MARK: - NFCNDEFReaderSessionDelegate
    
    /// - Tag: processingTagData
    func readerSession(_ session: NFCNDEFReaderSession, didDetectNDEFs messages: [NFCNDEFMessage]) {
        // Process detected NFCNDEFMessage objects.
        guard messages.count > 0 else {
            return
        }
        
        let message = messages[0]
        var parsedPayloads: [NdefPayload] = []
        for payload in message.records {
            if let parsed = self.parsePayload(payload) {
                parsedPayloads.append(parsed)
            }
        }
        guard parsedPayloads.count > 0 else {
            return
        }
        
        DispatchQueue.main.async {
            let payload = parsedPayloads[0]
            switch payload {
            case is NdefTextPayload:
                print("NdefTextPayload")
                let text = (payload as! NdefTextPayload).text
                guard let url = URL(string: "http://phdemo-project.herokuapp.com/\(text ?? "")") else {
                    
                    _ = self.showAlert(title: "NDEF message is not supported in this app.",
                                       message: nil,
                                       actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                    
                    break
                }
                
                let safariViewController = SFSafariViewController(url: url)
                safariViewController.delegate = self
                self.present(safariViewController, animated: true, completion: nil)
                
            case is NdefURIPayload:
                print("NdefURIPayload")
                guard let url = URL(string: (payload as! NdefURIPayload).URIString ?? "") else {
                    
                    _ = self.showAlert(title: "NDEF message is not supported in this app.",
                                       message: nil,
                                       actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                    
                    break
                }
                
                let safariViewController = SFSafariViewController(url: url)
                safariViewController.delegate = self
                self.present(safariViewController, animated: true, completion: nil)
                
            case is NdefSmartPosterPayload:
                print("NdefSmartPosterPayload")
                let spPayload = (payload as! NdefSmartPosterPayload)
                guard spPayload.messagePayloads.count > 0 else {
                    
                    _ = self.showAlert(title: "NDEF message is not supported in this app.",
                                       message: nil,
                                       actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                    
                    break
                }
                
                let subPayload = spPayload.messagePayloads[0]
                switch subPayload {
                case is NdefTextPayload:
                    let text = (subPayload as! NdefTextPayload).text
                    guard let url = URL(string: "http://phdemo-project.herokuapp.com/\(text ?? "")") else {
                        
                        _ = self.showAlert(title: "NDEF message is not supported in this app.",
                                           message: nil,
                                           actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                        
                        break
                    }
                    
                    let safariViewController = SFSafariViewController(url: url)
                    safariViewController.delegate = self
                    self.present(safariViewController, animated: true, completion: nil)
                    
                case is NdefURIPayload:
                    guard let url = URL(string: (subPayload as! NdefURIPayload).URIString ?? "") else {
                        
                        _ = self.showAlert(title: "NDEF message is not supported in this app.",
                                           message: nil,
                                           actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                        
                        break
                    }
                    
                    let safariViewController = SFSafariViewController(url: url)
                    safariViewController.delegate = self
                    self.present(safariViewController, animated: true, completion: nil)
                    
                default:
                    _ = self.showAlert(title: "NDEF format is not supported in this app.",
                                       message: nil,
                                       actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                    
                }
                
            default:
                _ = self.showAlert(title: "NDEF format is not supported in this app.",
                                   message: nil,
                                   actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                
            }
            
        }
        
    }
    
    /// - Tag: endScanning
    func readerSession(_ session: NFCNDEFReaderSession, didInvalidateWithError error: Error) {
        // Check the invalidation reason from the returned error.
        if let readerError = error as? NFCReaderError {
            // Show an alert when the invalidation reason is not because of a success read
            // during a single tag read mode, or user canceled a multi-tag read mode session
            // from the UI or programmatically using the invalidate method call.
            if (readerError.code != .readerSessionInvalidationErrorFirstNDEFTagRead)
                && (readerError.code != .readerSessionInvalidationErrorUserCanceled) {
                
                _ = self.showAlert(title: "Session Invalidated",
                                   message: error.localizedDescription,
                                   actions: UIAlertAction(title: "OK", style: .default, handler: nil))
                
            }
        }
        
        // A new session instance is required to read new tags.
        self.session = nil
    }
    
    // MARK: - SFSafariViewControllerDelegate
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        
    }
    
}


extension ViewController {
    
    // MARK: -- Orientation
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
}
